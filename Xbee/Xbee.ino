const int btn1 = 3;
const int btn2 = 4;
const int btn3 = 5;

bool isHigh = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(btn3, INPUT);

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(btn1) == HIGH) {
    sendToOtherDevice(1);
  } else if(digitalRead(btn2) == HIGH) {
    sendToOtherDevice(2);
  } else if(digitalRead(btn3) == HIGH) {
    sendToOtherDevice(3);
  } else {
    isHigh = false;
  }
} 

void sendToOtherDevice (int val) {
  if(!isHigh) {
    Serial.write(val);
    isHigh = true;
  }
}

