#include <Arduino.h>

#include <WiFi.h>

#include <HTTPClient.h>

#include <ArduinoJson.h>

#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`

#include <IRsend.h>

class WeatherDataPackage {
  public:
  String userName;
  String updatedAt;
  float temperature;
  float humidity;
  float lightIntensity;
  boolean precipitation;
  float windSpeed;
  
  WeatherDataPackage(){ 
    //the constructor 
  }
};

class IRDataPackage {
  public:
  int value;
  boolean received;
  
  IRDataPackage(){ 
    //the constructor 
  }
};

HTTPClient http;
WeatherDataPackage wdp;
IRDataPackage irdp;
SSD1306  display(0x3c, 4, 5);
IRsend irsend(12);

const char *ssid = "mentozer";
const char *password = "12345678";
// const char *ssid = "Wi-Fi-netwerk van Tim";
// const char *password = "123456789wifivantim";
String iotUrl = "http://iot-open-server.herokuapp.com/data";
String actuatorUrl = "https://stud.hosted.hr.nl/0894225/Jaar_4/WeatherStation/api/getActuator.php";
String deviceId = "59b10822abe4ea000475153b";
int displayCounter = -3;

void setup() {
  irsend.begin();
  Serial.begin(115200);
  Serial.flush();
  display.init();
  display.flipScreenVertically();
  showOnDisplay();
  delay(1000);
  connectToWifiNetwork();
}

void loop() {
  // put your main code here, to run repeatedly:
  String receivedDataString = getData(iotUrl);
  if(receivedDataString == "") {return;}
  dataToWeatherPackage(receivedDataString);
  showOnDisplay();
  
  String receivedActuatorString = getData(actuatorUrl);
  dataToIRDataPackage(receivedActuatorString);
  controlLight();
  delay(2000);
  String receivedActuatorString2 = getData(actuatorUrl);
  dataToIRDataPackage(receivedActuatorString2);
  controlLight();
  delay(2000);
}

void connectToWifiNetwork() {
  displayCounter = -2;
  showOnDisplay();
  Serial.println ( "Trying to establish WiFi connection with " + String(ssid));
  //wifiMulti.addAP(ssid, password);
  WiFi.begin ( ssid, password );
  Serial.println ( "" );

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  
//  // Wait for connection
//  while ( wifiMulti.run() != WL_CONNECTED ) {
//    delay ( 500 );
//    Serial.print ( "." );
//  }

  showOnDisplay();
  
  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );
}

String getData(String url) {
  bool failed = false;
  String payload;
  if(WiFi.status()== WL_CONNECTED){
    http.begin(url);
    int httpCode = http.GET();
    Serial.println(httpCode);
    if(httpCode == HTTP_CODE_OK) {
      payload = http.getString();
      Serial.println(payload);
    } else {
      failed = true;
    }
    http.end();
  } else {
    Serial.println("Wifi connection failed, retrying.");   
    connectToWifiNetwork();
  }
  if(!failed) {
     return payload;
  } else {
     return "";
  }
  
}

void dataToWeatherPackage(String dataString) {
  const size_t bufferSize = 10*JSON_ARRAY_SIZE(2) + 5*JSON_ARRAY_SIZE(3) + 5*JSON_ARRAY_SIZE(4) + 2*JSON_ARRAY_SIZE(5) + JSON_ARRAY_SIZE(6) + JSON_ARRAY_SIZE(7) + JSON_ARRAY_SIZE(24) + JSON_OBJECT_SIZE(1) + 25*JSON_OBJECT_SIZE(2) + 13*JSON_OBJECT_SIZE(3) + 66*JSON_OBJECT_SIZE(5) + 19*JSON_OBJECT_SIZE(6) + 11460;
  DynamicJsonBuffer jsonBuffer(bufferSize);
  JsonObject& root = jsonBuffer.parseObject(dataString);
  JsonArray& devices = root["devices"];
  Serial.println("Aantal devices");
  Serial.println(devices.size());
  for(int i = 0; i < devices.size(); i++) {
    JsonObject& device = devices[i];
    if(device["_id"].as<String>() == deviceId) {
      wdp.userName = device["name"].as<String>();
      wdp.updatedAt = device["updatedAt"].as<String>();
      wdp.temperature = device["data"][0]["value"].as<float>();
      wdp.humidity = device["data"][1]["value"].as<float>();
      wdp.lightIntensity = device["data"][2]["value"].as<float>();
      wdp.precipitation = device["data"][3]["value"].as<boolean>();
      wdp.windSpeed = device["data"][4]["value"].as<float>();
    }
  }
}

void dataToIRDataPackage(String dataString) {
  DynamicJsonBuffer jsonBuffer(200);
  JsonArray& devices = jsonBuffer.parseArray(dataString);
  Serial.println("Aantal devices");
  Serial.println(devices.size());
  for(int i = 0; i < devices.size(); i++) {
    JsonObject& device = devices[i];
    if(device["device_id"].as<String>() == deviceId) {
      irdp.value = device["value"].as<int>();
      irdp.received = device["received"].as<boolean>();
    }
  }
}

void showOnDisplay() {
  display.clear();
  display.setFont(ArialMT_Plain_24);
  String precipitationString = "Dry";
  if(wdp.precipitation) {
    precipitationString = "Rain / snow";
  }
  String updatedAtString = wdp.updatedAt;
  updatedAtString.replace("T", " ");
  updatedAtString.replace("Z", "");
  switch (displayCounter) {
    case -3:
      display.drawStringMaxWidth(0, 0, 128, "Please wait...");
      break;
    case -2:
      display.drawString(0, 0, "Connecting");
      display.drawString(0, 32, String(ssid));
      break;
    case -1:
      display.drawStringMaxWidth(0, 0, 128, "Waiting for data...");
      break;
    case 0:
      display.drawStringMaxWidth(0, 0, 128, wdp.userName);
      break;
    case 1:
      display.drawString(0, 0, "Updated");
      display.display();
      delay(1000);
      display.clear();
      display.drawStringMaxWidth(0, 0, 128, updatedAtString);
      break;
    case 2:
      display.drawString(0, 0, "Temperature");
      display.drawString(0, 32, String(wdp.temperature, 1) + " " + "\xDF" + "C");
      break;
    case 3:
      display.drawString(0, 0, "Humidity");
      display.drawString(0, 32, String(wdp.humidity, 1) + "%");
      break;
    case 4:
      display.drawString(0, 0, "Light Intensity");
      display.drawString(0, 32, String(wdp.lightIntensity, 1) + "%");
      break;
    case 5:
      display.drawString(0, 0, "Precipitation");
      display.drawString(0, 32, precipitationString);
      break;
    case 6:
      display.drawString(0, 0, "Wind speed");
      display.drawString(0, 32, String(wdp.windSpeed, 1) + "km/h");
      break;
    default: 
      // if nothing else matches, do the default
      // default is optional
    break;
  }
  display.display();
  displayCounter++;
  if(displayCounter > 6) {
    displayCounter = 0;
  }
  Serial.println(wdp.userName);
}

void controlLight() {
  if(!irdp.received) {
    Serial.println("Sending IR signal " + String(irdp.value));
    switch (irdp.value) {
      case 1:
        //On off
        irsend.sendNEC(0xFF02FD, 32);
      break;
      case 2:
        //DIY1
        irsend.sendNEC(0xFF30CF, 32);
      break;
      case 3:
        //Jump
        irsend.sendNEC(0xFF20DF, 32);
      break;
      case 4:
        //Fade
        irsend.sendNEC(0xFF609F, 32);
      break;
      default: 
        // if nothing else matches, do the default
        // default is optional
      break;
    }
  }
  
}

