#include <IRremote.h>

IRsend irsend;

void setup()
{
  Serial.begin(9600);
  Serial.println("starting");
}

void loop() {
  Serial.println("trying");
//  irsend.sendNEC(0xFF02FD, 32);
//  
//  Serial.println("trying");
//
//  delay(5000); //5 second delay between each signal burst

  irsend.sendNEC(intToRemoteVal(0), 32);
  delay(5000);

  irsend.sendNEC(intToRemoteVal(1), 32);
  delay(5000);
  irsend.sendNEC(intToRemoteVal(2), 32);
  delay(5000);
  irsend.sendNEC(intToRemoteVal(3), 32);
  delay(5000);
  
  irsend.sendNEC(intToRemoteVal(0), 32);
  delay(5000);
}

int intToRemoteVal(int val) {
  int returnVal;
  switch (val) {
    case 0:
      //On off
      Serial.println("Onoff");
      returnVal = 0xFF02FD;
      break;
    case 1:
      //default color (DIY1)
      Serial.println("DIY1");
      returnVal = 0xFF30CF;
      break;
    case 2:
      //Red
      Serial.println("jump");
      returnVal = 0xFF20DF;
      break;
    case 3:
      //Green
      Serial.println("fade");
      returnVal = 0xFF609F;
      break;
    default: 
      //nothing
      break;
  }
  return returnVal;
}

