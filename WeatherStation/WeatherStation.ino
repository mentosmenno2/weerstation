#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include "DHT.h"

#define DHTPIN D4
#define PRECIPITATIONPIN D3
#define WINDSPEEDPIN D2
#define DHTTYPE DHT22
#define LDRPIN A0

DHT dht(DHTPIN, DHTTYPE);
HTTPClient http;

const char *ssid = "mentozer";
const char *password = "12345678";
// const char *ssid = "Wi-Fi-netwerk van Tim";
// const char *password = "123456789wifivantim";
String httpUrl = "http://iot-open-server.herokuapp.com/data";
String apiToken = "4f009b2cd84340c147867cf8";

void setup() {
  Serial.begin(115200);
  dht.begin();

  pinMode(LDRPIN, INPUT);
  pinMode(WINDSPEEDPIN, INPUT);
  
  connectToWifiNetwork();
}

void loop() {
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  float lightIntensity = readLightInensity();
  bool precipitation = readPrecipitation();
  float windSpeed = readWindSpeed();
  // Serial.println("Windspeed");
  // Serial.println(windSpeed);

  StaticJsonBuffer<400> jsonBuffer;
  JsonObject& jsonRoot = jsonBuffer.createObject();
  
  jsonRoot["token"] = apiToken;
  
  JsonArray& data = jsonRoot.createNestedArray("data");

  if(humidity == humidity) {
    JsonObject& humidityObject = jsonBuffer.createObject();
    humidityObject["key"] = "humidity";
    humidityObject["value"] = humidity;
    data.add(humidityObject);
  }

  if(temperature == temperature) {
    JsonObject& temperatureObject = jsonBuffer.createObject();
    temperatureObject["key"] = "temperature";
    temperatureObject["value"] = temperature;
    data.add(temperatureObject);
  }
  
  if(lightIntensity == lightIntensity) {
    JsonObject& lightIntensityObject = jsonBuffer.createObject();
    lightIntensityObject["key"] = "lightIntensity";
    lightIntensityObject["value"] = lightIntensity;
    data.add(lightIntensityObject);
  }

  if(precipitation == precipitation) {
    JsonObject& precipitationObject = jsonBuffer.createObject();
    precipitationObject["key"] = "precipitation";
    precipitationObject["value"] = precipitation;
    data.add(precipitationObject);
  }

  if(windSpeed == windSpeed) {
    JsonObject& windSpeedObject = jsonBuffer.createObject();
    windSpeedObject["key"] = "windSpeed";
    windSpeedObject["value"] = windSpeed;
    data.add(windSpeedObject);
  }

  String dataToSend;
  jsonRoot.printTo(dataToSend);

  Serial.println(dataToSend);
  
  postData(dataToSend);
  delay(500);
}

void connectToWifiNetwork() {
  Serial.println ( "Trying to establish WiFi connection with " + String(ssid));
  WiFi.begin ( ssid, password );
  Serial.println ( "" );
  
  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }

  Serial.println ( "" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );

//  if ( MDNS.begin ( "esp8266" ) ) {
//    Serial.println ( "MDNS responder started" );
//  }
}

void postData(String stringToPost) {
  if(WiFi.status()== WL_CONNECTED){
    http.begin(httpUrl);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(stringToPost);
    String payload = http.getString();
  
    Serial.println(httpCode);
    Serial.println(payload);
    http.end();
  } else {
    Serial.println("Wifi connection failed, retrying.");   
    connectToWifiNetwork();
  }
}

float readLightInensity() {
   float ldrIn = analogRead(LDRPIN);
   return ldrIn / 10;
}

bool readPrecipitation() {
  bool prec = digitalRead(PRECIPITATIONPIN);
  return !prec;
}

float readWindSpeed() {
  int state, prevState = 0;
  int counter = 0;
  int timeToRead = 1000;
  float startTime = millis();

  while((millis() - startTime) < timeToRead) {
    state = digitalRead(WINDSPEEDPIN);
    delay(1); //Hacky solution to prevent false readings

    if(state != prevState)
    {
      counter++;
      prevState = state;
    }
  }

  counter = counter / 2; //= rounds p sec
  int rpm = counter * (60000 / timeToRead);
  return rpm; //should become km/h
}